require "csv"

namespace :integrations do
  task :create_product_imports => :environment do
    products_count = ENV['count'].blank? ? 1000 : ENV['count'].to_i

    #id, vendor, title, description, sku price, taxable,  weight,  inventory_item_id, inventory_quantit, old_inventory_quantity,  inventory_policy
    CSV.open("shopify.csv", "wb") do |csv|
      products_count.times do |i|
        csv << ["#{i}", "vendor-#{i}", "title-#{i}", "description-#{i}", "sku-#{i}", "#{i + rand(10)}", "#{rand(2) == 1}", "#{rand(100)}", "#{i}", "#{i}", "#{i}", "policy-#{i}"]
      end
    end

    #id, sku, product_type, title, description,  price, weight, weight_unit, barcode, grams, image_id
    CSV.open("big_commerce.csv", "wb") do |csv|
      products_count.times do |i|
        csv << ["#{i}", "sku-#{i}", "type-#{i}", "title-#{i}", "description-#{i}", "#{i + rand(10)}", "#{rand(100)}", "unit-#{i}", "barcode-#{i}", "#{rand(10)}", "#{i}"]
      end
    end

    #id,  type,  weight, title, description,  price,  requires_shipping, position, sku
    CSV.open("net_suite.csv", "wb") do |csv|
      products_count.times do |i|
        csv << ["#{i}", "type-#{i}", "#{rand(100)}", "title-#{i}", "description-#{i}", "#{i + rand(10)}", "#{rand(2) == 1}", "#{rand(100)}", "sku-#{i}"]
      end
    end

  end
end
