class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.json :attachment
      t.references :integration, null: false, foreign_key: true

      t.timestamps
    end
  end
end
