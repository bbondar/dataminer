class DocumentsController < ApplicationController

  def new
    @document = Document.new
  end

  def create
    document = Document.new
    UploadDocumentService.call(document, document_params[:attachment])
    redirect_to root_url
  end

  private

  def document_params
    params.require(:document)
          .permit(:attachment)
  end
end
