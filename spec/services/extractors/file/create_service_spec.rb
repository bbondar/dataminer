require 'rails_helper'

describe Extractors::File::CreateService do

  subject { described_class.call(integration) }

  let(:shopify_attachment) { fixture_file_upload('files/shopify.csv', 'text/csv') }
  let(:big_commerce_attachment) { fixture_file_upload('files/big_commerce.csv', 'text/csv') }
  let(:net_suite_attachment) { fixture_file_upload('files/net_suite.csv', 'text/csv') }

  let!(:shopify_integration) { create(:shopify_integration) }
  let!(:net_suite_integration) { create(:net_suite_integration) }
  let!(:big_commerce_integration) { create(:big_commerce_integration) }

  let(:products) { Product.all }

  before do
    integration.documents << document
  end
  context 'when shopify integration' do

    let(:integration) { shopify_integration }
    let(:document) { Document.create(attachment: shopify_attachment) }

    it 'should create products with correct fields' do
      expect{ subject }.to change{ products.count }.from(0).to(2000)
    end
  end

  context 'when net_suite integration' do

    let(:integration) { net_suite_integration }
    let(:document) { Document.create(integration: integration.reload, attachment: shopify_attachment) }

    it 'should create products with correct fields' do
      integration.reload.documents
      expect{ subject }.to change{ products.count }.from(0).to(2000)
    end
  end

  context 'when big_commerce integration' do

    let(:integration) { big_commerce_integration }
    let(:document) { Document.create(integration: integration.reload, attachment: shopify_attachment) }

    it 'should create products with correct fields' do
      integration.reload.documents
      expect{ subject }.to change{ products.count }.from(0).to(2000)
    end
  end
end
