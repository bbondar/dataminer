module CallableService
  extend ActiveSupport::Concern

  module ClassMethods
    def call(*args, &block)
      new(*args, &block).call
    end
  end
end
