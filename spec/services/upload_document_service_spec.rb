require 'rails_helper'

describe UploadDocumentService do

  subject { described_class.call(document, attachment) }


  let(:document) { Document.new }

  let(:shopify_attachment) { fixture_file_upload('files/shopify.csv', 'text/csv') }
  let(:big_commerce_attachment) { fixture_file_upload('files/big_commerce.csv', 'text/csv') }
  let(:net_suite_attachment) { fixture_file_upload('files/net_suite.csv', 'text/csv') }

  let!(:shopify_integration) { create(:shopify_integration) }
  let!(:net_suite_integration) { create(:net_suite_integration) }
  let!(:big_commerce_integration) { create(:big_commerce_integration) }


  let(:attachment) { shopify_attachment }
  it 'should save document' do
    subject
    expect(document).not_to be_a_new(Document)
  end

  context 'when upload shopify attachment' do
    let(:attachment) { shopify_attachment }

    it 'should create document to proper integration' do
      subject
      expect(document.integration).to eq(shopify_integration)
    end
  end

  context 'when upload net_suite attachment' do
    let(:attachment) { net_suite_attachment }

    it 'should create document to proper integration' do
      subject
      expect(document.integration).to eq(net_suite_integration)
    end
  end

  context 'when upload big_commerce attachment' do
    let(:attachment) { big_commerce_attachment }

    it 'should create document to proper integration' do
      subject
      expect(document.integration).to eq(big_commerce_integration)
    end
  end
end

