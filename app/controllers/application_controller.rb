class ApplicationController < ActionController::Base

  rescue_from StandardError do |e|
    Rails.logger.debug ([e.message]+e.backtrace).join($/)
    render json: e.message, status: 500
  end
end
