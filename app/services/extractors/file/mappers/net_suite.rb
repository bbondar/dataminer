class Extractors::File::Mappers::NetSuite
  include CallableService

  def initialize(lines)
    @lines = lines
  end

  def call
    lines.map do |line|
      items = line.split(',')
      {
        title: items[3],
        description: items[4],
        weight: items[3],
        price: items[3],
        sku: items[3],
        external_id: items[0]
      }
    end
  end

  private

  attr_accessor :lines

end

