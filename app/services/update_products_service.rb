class UpdateProductsService
  include CallableService

  def initialize(integration_ids)
    @integration_ids = integration_ids
  end

  def call
    integrations = Integration.where(id: integration_ids)
    integrations.each do |integration|
      Extractors::File::CreateService.call(integration)
    end
  end

  private

  attr_accessor :integration_ids

end
