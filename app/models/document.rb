class Document < ApplicationRecord
  belongs_to :integration

  mount_uploader :attachment, ProductAttachmentUploader
end
