require 'rails_helper'

describe UpdateProductsService do

  subject { described_class.call(integration_ids) }

  let!(:shopify_integration) { create(:shopify_integration) }
  let!(:net_suite_integration) { create(:net_suite_integration) }
  let!(:big_commerce_integration) { create(:big_commerce_integration) }

  context 'when selected subset integrations' do
    let(:integration_ids) { [shopify_integration.id, net_suite_integration.id] }
    it 'should update proper integrations' do
      expect(Extractors::File::CreateService).to receive(:call).twice
      subject
    end
  end

  context 'when selected all integrations' do
    let(:integration_ids) { [shopify_integration.id, net_suite_integration.id, big_commerce_integration.id] }
    it 'should update proper integrations' do
      expect(Extractors::File::CreateService).to receive(:call).exactly(3).times
      subject
    end
  end
end
