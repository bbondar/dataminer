FactoryBot.define do
  factory :integration do
    factory :shopify_integration do
      name { 'shopify' }
    end

    factory :net_suite_integration do
      name { 'net_suite' }
    end

    factory :big_commerce_integration do
      name { 'big_commerce' }
    end
  end
end

