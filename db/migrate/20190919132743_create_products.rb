class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :title
      t.string :description
      t.decimal :price, precision: 8, scale: 2, null: false
      t.string :sku, null: false
      t.float :weight, null: false
      t.string :external_id, null: false

      t.timestamps  default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :products, [:sku, :external_id], unique: true
  end
end
