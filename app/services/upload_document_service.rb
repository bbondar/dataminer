class UploadDocumentService
  include CallableService

  class UploadError < StandardError; end

  def initialize(document, attachment)
    @document = document
    @attachment = attachment
  end

  def call
    integration_name = attachment.original_filename.gsub('.csv', '')
    integration = Integration.find_by(name: integration_name)

    unless document.update(attachment: attachment, integration: integration)
      raise UploadError, document.errors.messages
    end
  end

  private

  attr_accessor :document, :attachment

end
