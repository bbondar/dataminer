FactoryBot.define do
  factory :product do
    sequence(:title) { |n| "title-#{n}" }
    sequence(:description) { |n| "description-#{n}" }
    sequence(:price)
  end
end
