class Extractors::File::CreateService
  include CallableService

  class NoDataToExtract < StandardError; end

  MAPPERS = {
    'big_commerce': Extractors::File::Mappers::BigCommerce,
    'shopify':      Extractors::File::Mappers::Shopify,
    'net_suite':    Extractors::File::Mappers::NetSuite,
  }

  def initialize(integration)
    @integration = integration
  end

  def call
    if file_path = integration.documents.last&.attachment&.current_path
      File.open(file_path, 'r') do |file|
        file.lazy.each_slice(1000) do |lines|
          products = mapper_class.call(lines)
          Product.insert_all(products)
        end
      end
    else
      raise NoDataToExtract, "#{integration.name} integration has no file to extract"
    end
  end

  private

  attr_accessor :integration

  def mapper_class
    MAPPERS[integration.name.to_sym]
  end

end
