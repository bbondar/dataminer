class ImportsController < ApplicationController

  def index
    @integrations = Integration.all
  end

  def create
    UpdateProductsService.call(import_params[:integration_ids])
    redirect_to root_url
  end

  def import_params
    params.require(:import)
          .permit(integration_ids:[])
  end
end
